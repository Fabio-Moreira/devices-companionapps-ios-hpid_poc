//
//  ViewController.swift
//  HPIDExample
//
//  Created by Fábio Osório Moreira on 3/15/16.
//  Copyright © 2016 HP. All rights reserved.
//

import UIKit
import WebKit
import Alamofire
import SWXMLHash

class ViewController: UIViewController, WKNavigationDelegate {
    
    private var webView: WKWebView?
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var newAccountButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    
    let callBackUrl = "https://localhost/login/callback"
    let tokenUrl = "https://webauth-pie1.hpconnectedpie.com/oauth/puma/token?grant_type=authorization_code&client_id=e01f80420a6f11e5b57300215a989b02&response_type=token&code="
    let wpIdUrl = "https://webauth-pie1.hpconnectedpie.com/oauth/puma/validate"
    let emailUrl = "https://webauth-pie1.hpconnectedpie.com/pam/services/pam/users/userIdentity="
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        webView = WKWebView()
        webView!.navigationDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func newAccountButtonTouched(sender: AnyObject) {
        print ("New Account Button touched")
        //let originalurl = "https://webauth-pie1.hpconnectedpie.com/oauth/ui/authorizeweb?client_id=e01f80420a6f11e5b57300215a989b02&redirect_uri=https://localhost/login/callback&response_type=code&theme=DKTBR"
        
        if let signUpUrl = NSURL(string:"https://webauth-pie1.hpconnectedpie.com/oauth/ui/signUp?client_id=e01f80420a6f11e5b57300215a989b02&response_type=code&state=&redirect_uri=" + callBackUrl + "&theme=RWD&overlay=false&forceLogin=false&country_changeable=true") {
            let req = NSURLRequest(URL: signUpUrl)
            webView!.loadRequest(req)
            view = webView
        }
    }
    
    @IBAction func loginButtonTouched(sender: AnyObject) {
        print ("Login Button touched")
        if let signInUrl = NSURL(string: "https://webauth-pie1.hpconnectedpie.com/oauth/ui/login?client_id=e01f80420a6f11e5b57300215a989b02&response_type=code&state=&redirect_uri=" + callBackUrl + "&theme=RWD&overlay=false&forceLogin=false&country_changeable=true") {
            let req = NSURLRequest(URL: signInUrl)
            webView!.loadRequest(req)
            view = webView
        }
    }
    
    @IBAction func resetButtonTouched(sender: AnyObject) {
        print ("Reset Cache button touched")
        print ("Clearing Cache")
        let websiteDataTypes = NSSet(array: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache])
        let date = NSDate(timeIntervalSince1970: 0)
        WKWebsiteDataStore.defaultDataStore().removeDataOfTypes(websiteDataTypes as! Set<String>, modifiedSince: date, completionHandler:{ })
    }
    
    func getQueryStringParameter(url: String?, param: String) -> String? {
        if let url = url, urlComponents = NSURLComponents(string: url), queryItems = (urlComponents.queryItems! as [NSURLQueryItem]?) {
            return queryItems.filter({ (item) in item.name == param }).first?.value!
        }
        return nil
    }
    
    
    
    // MARK: WKNavigationDelegate

    func webView(webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        let urlString = String(webView.URL!)
        print ("Redirect received: " + urlString)
        
        if urlString.hasPrefix(callBackUrl) {
            print ("Tokens ready")
            let authCode = getQueryStringParameter(urlString, param: "authcode")
            print ("AuthCode=" + authCode!)
            // Tokens should be here...
            let tokensUrl: String = tokenUrl + authCode!
            print ("Getting token from " + tokensUrl)
            let user = "e01f80420a6f11e5b57300215a989b02"
            let password = "41f2cfe6-0a70-11e5-9b1f-005056002ef3"
            // Damn you ascii format
            let credentialData = "\(user):\(password)".dataUsingEncoding(NSUTF8StringEncoding)!
            let base64Credentials = credentialData.base64EncodedStringWithOptions([])
            let headers = ["Authorization": "Basic \(base64Credentials)"]
            Alamofire.request(.POST, tokensUrl, headers: headers)
                .responseJSON { response in switch response.result {
                case .Success(let JSON):
                    let response = JSON as! NSDictionary
                    
                    //example if there is an id
                    let accessToken = response.objectForKey("access_token")! as! String
                    let refreshToken = response.objectForKey("refresh_token")! as! String
                    let tokenExpiration = response.objectForKey("expires_in")!
                    print ("Access Token = " + accessToken)
                    print ("Refresh Token = " + refreshToken)
                    print ("Expires In = " + (String(tokenExpiration)))
                    
                    //let accessTokenInAscii = accessToken as! String).dataUsingEncoding(NSASCIIStringEncoding)!
                    //let base64AccessToken = accessToken.base64EncodedStringWithOptions([])
                    
                    let authHeaders = ["Authorization": "Bearer " + accessToken]
                    Alamofire.request(.POST, self.wpIdUrl, headers: authHeaders)
                        .response { (request, response, data, error) in
                            let wpIdXml = SWXMLHash.parse(data!)
                            let wpId = wpIdXml["tokenInformation"]["uid"].element?.text
                            print ("wpId = " + wpId!)
                            
                            Alamofire.request(.GET, self.emailUrl+wpId!, headers: authHeaders)
                                .response { (request, response, data, error) in
                                    let emailXml = SWXMLHash.parse(data!)
                                    let email = emailXml["userIdentity"]["emailAddress"].element?.text
                                    print ("Email = " + email!)
                                    
                                    print ("Clearing Cache")
                                    let websiteDataTypes = NSSet(array: [WKWebsiteDataTypeCookies, WKWebsiteDataTypeSessionStorage])
                                    let date = NSDate(timeIntervalSince1970: 0)
                                    WKWebsiteDataStore.defaultDataStore().removeDataOfTypes(websiteDataTypes as! Set<String>, modifiedSince: date, completionHandler:{ })
                                /*
                                .responseJSON { response in switch response.result {
                                    case .Success(let JSON):
                                        print("Success with JSON: \(JSON)")
                                    case .Failure(let error):
                                        print("Request failed with error: \(error)")
                                    }*/
                            }
                            
                    }

                    
                    
                    
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    }
            }
        }
    }
        
    func webView(webView: WKWebView, didCommitNavigation navigation: WKNavigation!) {
        print ("Did commit navigation: " + String(webView.URL))
    }
}

